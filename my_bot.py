from chatterbot.trainers import ListTrainer
from chatterbot import ChatBot


#create bot instance from Chatbot
bot = ChatBot(
    "Botty",
    storage_adapter = "chatterbot.storage.SQLStorageAdapter",
    logic_adapters = [
        {
            'import_path': 'chatterbot.logic.BestMatch',
            'default_response': 'I am sorry, but I don\'t understand what you are saying. I\'ll let my trainer know.',
            'maximum_similarity_threshold': 0.90
        }
    ],
    database_uri = "sqlite:///database.sqlite3"

)

# while True:
#     try:
#         bot_input =  bot.get_response(input())
#         print(bot_input)
#     except(KeyboardInterrupt, EOFError, SystemExit):
#         break

#create a list of conversation to train bot
conversation = [
    "Hi",
    "Hi there",
    "Why?",
    "Why what?",
    "Goodbye",
    "See you soon",
    "Who are you",
    "I am a librarian bot.",
    "You are smart.",
    "Thanks"
]

#create a ListTrainer instance from bot instance
trainer = ListTrainer(bot)

#train the bot with conversation list
trainer.train(conversation)

def bot_reply(text):
    """reply user message with trained conversation"""
    response = bot.get_response(text)
    return response.__str__()
