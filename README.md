# library_bot

A messenger that interact with uses as a librarian.

**To run this program**
install packages requirements.txt
change VERIFY_TOKEN and PAGE_ACCESS_TOKEN for your page
enter your call back urls for your webhook
run flask with this command
-for windows `set FLASK_APP=chatbot.py`, `flask run`
-for linux  `export FLASK_APP=chatbot.py`, `flask run`
verify webhook


**Notice**
chatterbot support python 3.7.* and below
If there is an error in installing en-core-web-sm==2.3.1 from requirements,
please try download separately with this command `python -m spacy download en_core_web_sm`

**VERIFY_TOKEN** and **PAGE_ACCESS_TOKEN** must be changed for your page.
valid call back url is needed to be added to Facebook webhook and verified.
