import requests

PAGE_ACCESS_TOKEN = '''EAADZAa3j1DyEBAMpuW9WVloBvM9spmyyWLQqJxIXz0VFRDSwTrfNHVMv7ijDmDj
D8XiuYMSwm8525GywZAJcZAzQbkfWwANsLoZBh0t6tqkf4ZB3XYtFKVwI98vjldJfcXZBlZCWUpf9dJZA3wt79g
fk6eYaZAneSlwODEQY2ljl6nXX1Dr0ZB4pp8TFDarTZB6yn4ZD'''

request_endpoint = "https://graph.facebook.com/v8.0/me/messenger_profile"
auth = {
    'access_token': PAGE_ACCESS_TOKEN
}


def set_get_started(gs_obj):
        """Set a get started button shown on welcome screen for first time users
        https://developers.facebook.com/docs/messenger-platform/reference/messenger-profile-api/get-started-button
        Input:
          gs_obj: Your formatted get_started object as described by the API docs
        Output:
          Response from API as <dict>
        """
        response = requests.post(
            request_endpoint,
            params = auth,
            json = gs_obj
        )
        result = response.json()
        return result

def set_greeting(greeting_obj):
    """set greeting button in messenger proifile
    greeting_obj can be found at facebook doc for messenger platform"""
    response = requests.post(request_endpoint, params=auth, json=greeting_obj)
    result = response.json()
    return result

def delete_msgr_profile(field_list):
    """delete messenger profile and field_list is list of profile fields"""
    delete_obj = {"fields": field_list}
    response = requests.delete(request_endpoint, params=auth, json=delete_obj)
    result = response.json()
    return result

def set_persistent_menu(pm_obj):
        """Set a persistent_menu that stays same for every user. Before you can use this, make sure to have set a get started button.
        https://developers.facebook.com/docs/messenger-platform/reference/messenger-profile-api/persistent-menu
        Input:
          pm_obj: Your formatted persistent menu object as described by the API docs
        Output:
          Response from API as <dict>
        """
        response = requests.post(
            request_endpoint,
            params = auth,
            json = pm_obj
        )
        result = response.json()
        return result

def send_typing_noti(recipient_id, action='typing_on'):
    """send typing notification(typing_on, typing_off, mark_seen) to the user"""
    response = requests.post(
        url = "https://graph.facebook.com/v2.6/me/messages",
        params = auth,
        json = {
            'recipient': {
                'id': recipient_id
            },
            'sender_action': action
        }
    )
    result = response.json()
    return result
