import re
from pymessenger import Bot
from flask import Flask, request
from witapp import reply,template_message
from my_bot import bot_reply
from utils import set_get_started,set_greeting,delete_msgr_profile, set_persistent_menu, send_typing_noti

#initiate flask app
app = Flask(__name__)
#assign verify token and page access token
VERIFY_TOKEN = 'token_for_library_bot'
PAGE_ACCESS_TOKEN = '''EAADZAa3j1DyEBAMpuW9WVloBvM9spmyyWLQqJxIXz0VFRDSwTrfNHVMv7ijDmDj
D8XiuYMSwm8525GywZAJcZAzQbkfWwANsLoZBh0t6tqkf4ZB3XYtFKVwI98vjldJfcXZBlZCWUpf9dJZA3wt79g
fk6eYaZAneSlwODEQY2ljl6nXX1Dr0ZB4pp8TFDarTZB6yn4ZD'''
#create pymessenger bot instance
bot = Bot(PAGE_ACCESS_TOKEN)

#create a list of categories
categories = ['fiction', 'science', 'drama']
#create a library dictionary
library = {
    'fiction': ['harry potter', 'lord of the rings', 'game of thrones'],
    'science': ['a brief history of time', 'a life on our planet', 'the handy science'],
    'drama': ['anna', 'before we were yours', 'tears of endurance']
    }

# #get started object
# getstarted = {
#     'get_started': {
#         'payload': 'Get Started'
#     }
# }
#
# #send get started button
#
# result = set_get_started(getstarted)
# print(result, 'outside')

# greeting = {
#     "greeting":[
#   {
#     "locale":"default",
#     "text":"Hello {{user_first_name}}!"
#   }
# ]
# }
#
# result = set_greeting(greeting)
# print(result)
# result = delete_msgr_profile(['greeting'])
# print(result)

# pm_obj = {
#     "persistent_menu": [
#         {
#             "locale": "default",
#             "composer_input_disabled": False,
#             "call_to_actions": [
#                 {
#                     "type": "postback",
#                     "title": "Talk to a librian",
#                     "payload": "CARE_HELP"
#                 },
#                 {
#                     "type": "postback",
#                     "title": "Choose language",
#                     "payload": "CURATION"
#                 },
#                 {
#                     "type": "web_url",
#                     "title": "Read a book now",
#                     "url": "https://www.librarybot.com",
#                     "webview_height_ratio": "full"
#                 }
#             ]
#         }
#         ]
#     }
#
# result = set_persistent_menu(pm_obj)
# print(result)

@app.route('/', methods=['GET'])
def verify():
    """webhook verification"""
    if request.args.get("hub.mode") == 'subscribe' and request.args.get("hub.challenge"):
        if not request.args.get("hub.verify_token") == VERIFY_TOKEN:
            return "verification fail", 403
        return request.args["hub.challenge"], 200
    return 'ok', 200


@app.route('/', methods=['POST'])
def webhook():
    """interact with user through facebook messenger webhook"""
    #get json data from the webhook response
    data = request.get_json()

    if request.method == 'POST':

        if data['object'] == 'page':

            for entry in data['entry']:

                for message_data in entry['messaging']:
                    #assign sender_id and recipient_id from json data
                    sender_id = message_data['sender']['id']
                    recipient_id = message_data['recipient']['id']
                    #if message is included in json
                    if message_data.get('message'):
                        if 'text' in message_data['message']:
                            #extract text from json
                            text = message_data['message']['text']
                        else:
                            #if there is no text set text to 'Hi there'
                            text = "Hi there"

                        #set response to None
                        response = None
                        #get entity and value from user message through wit.ai
                        entity, value = reply(text)
                        #if there is a match, response back with generic template message
                        if entity == "search_books:search_books" and value == "books":
                            response = template_message(categories)

                            send_typing_noti(sender_id, action='mark_seen')
                            send_typing_noti(sender_id)
                            bot.send_text_message(sender_id, 'You can search books through these categories.')
                            send_typing_noti(sender_id)
                            bot.send_generic_message(sender_id, response)
                        #if there is not a match, reply with train conversation from catterbot
                        else:
                            response = bot_reply(text)
                            send_typing_noti(sender_id, action='mark_seen')
                            send_typing_noti(sender_id)
                            bot.send_text_message(sender_id, response)

                    #if postback is included in json, make responses based on language
                    elif message_data.get('postback'):
                        #change postback message to lower case to be consistant
                        postback_title = message_data['postback']['title'].lower()
                        lang = 'en'
                        #if there is no english character in it, set lang to 'mm'
                        if not re.search(r'^[a-zA-Z0-9_]+$', postback_title):
                            lang = 'mm'

                        if postback_title == 'get started' or postback_title == 'choose language':
                            text = "Choose Your language\nဘာသာစကားရွေးချယ်ပါ"
                            #create a list of  postback buttons to send as button message
                            choose_language_button = [
                                                        {
                                                            'type':'postback',
                                                            'title': 'English',
                                                            'payload': 'DEVELOPER_DEFINED_PAYLOAD'
                                                        },
                                                        {
                                                            'type':'postback',
                                                            'title': 'မြန်မာ',
                                                            'payload': 'DEVELOPER_DEFINED_PAYLOAD'
                                                        }
                                                    ]
                            send_typing_noti(sender_id, action='mark_seen')
                            send_typing_noti(sender_id)
                            bot.send_button_message(sender_id, text, choose_language_button)

                        elif postback_title == 'english':
                            text = "Please click \"Choose Category\" to choose"
                            #create a list of  postback buttons to send as button message
                            choose_language_button = [
                                                        {
                                                            'type':'postback',
                                                            'title': 'Choose Category',
                                                            'payload': 'DEVELOPER_DEFINED_PAYLOAD'
                                                        }
                                                    ]
                            send_typing_noti(sender_id, action='mark_seen')
                            send_typing_noti(sender_id)
                            bot.send_text_message(sender_id, "Hi there.\nHow may I help you.")
                            send_typing_noti(sender_id)
                            bot.send_button_message(sender_id, text, choose_language_button)
                        #send generic template message with categories list
                        elif postback_title == 'choose category':
                            response = template_message(categories)
                            send_typing_noti(sender_id, action='mark_seen')
                            send_typing_noti(sender_id)
                            bot.send_generic_message(sender_id, response)
                        #send generic template message for books of one category
                        elif postback_title == 'science books' or postback_title == 'သိပ္ပံစာအုပ်များ':
                            category_list = []
                            category_list.append('science')
                            response = template_message(category_list, library=library)
                            send_typing_noti(sender_id, action='mark_seen')
                            send_typing_noti(sender_id)
                            bot.send_generic_message(sender_id, response)
                        #send generic template message for books of one category
                        elif postback_title == 'fiction books' or postback_title == 'စိတ်ကူးယဉ်စာအုပ်များ':
                            category_list = []
                            category_list.append('fiction')
                            response = template_message(category_list, library=library)
                            send_typing_noti(sender_id, action='mark_seen')
                            send_typing_noti(sender_id)
                            bot.send_generic_message(sender_id, response)
                        #send generic template message for books of one category
                        elif postback_title == 'drama books' or postback_title == 'ဒရာမာစာအုပ်များ':
                            category_list = []
                            category_list.append('drama')
                            response = template_message(category_list, library=library)
                            send_typing_noti(sender_id, action='mark_seen')
                            send_typing_noti(sender_id)
                            bot.send_generic_message(sender_id, response)

                        elif postback_title == 'မြန်မာ':
                            text = 'စာအုပ်အမျိုးအစား ရွေးရန် "အမျိုးအစားမျာ" ကိုနှိပ်ပါ။'
                            #create a list of  postback buttons to send as button message for myanmar language
                            choose_language_button = [
                                                        {
                                                            'type':'postback',
                                                            'title': 'အမျိုးအစားများ',
                                                            'payload': 'DEVELOPER_DEFINED_PAYLOAD'
                                                        }
                                                    ]
                            send_typing_noti(sender_id, action='mark_seen')
                            send_typing_noti(sender_id)
                            bot.send_text_message(sender_id, 'မင်္ဂလာပါ။')
                            send_typing_noti(sender_id)
                            bot.send_button_message(sender_id, text, choose_language_button)
                        #send generic template message with categories list for myanmar language
                        elif postback_title == 'အမျိုးအစားများ':
                            response = template_message(categories, lang=lang)
                            send_typing_noti(sender_id, action='mark_seen')
                            send_typing_noti(sender_id)
                            bot.send_generic_message(sender_id, response)
                        #send text message for each book
                        elif postback_title in [book for category in library for book in library[category]]:
                            text = 'Please go to www.librarybot.com to read ' + postback_title.upper() + '\n\n' + postback_title.upper() + ' ကိုဖတ်ရန် www.librarybot.com ကိုနှိပ်ပါ။'
                            send_typing_noti(sender_id, action='mark_seen')
                            send_typing_noti(sender_id)
                            bot.send_text_message(sender_id, text)
                        #send reply message for one button of persistent menu
                        elif postback_title == "talk to a librian":
                            text = 'The librarian will reply your message soon\n\nစာကြည်တိုက်မှုးက မကြာခင် အကြောင်းပြန်ပါလိမ့်မယ်။'
                            send_typing_noti(sender_id, action='mark_seen')
                            send_typing_noti(sender_id)
                            bot.send_text_message(sender_id, text)
    return 'ok', 200






if __name__ ==  "__main__":
    app.run(debug=True, port=80)
