from wit import Wit
from flask import url_for

SERVER_ACCESS_TOKEN = 'A2HA2NMMQV3NV6ZJBJVNBGMIPJTRNY5A'

#create an instance of Wit
client = Wit(SERVER_ACCESS_TOKEN)

def reply(message):
    """guess user message to entity and value that have been defined on wit.ai
    and return them. If there is no matches return None."""
    response = client.message(message)
    entity = None
    value = None

    try:
        entity = list(response['entities'])[0]
        value = response['entities'][entity][0]['value']
    except:
        pass

    return (entity, value)

def template_message(categories, library=None, lang='en'):
    """return element list of generiic template payload based on parameters
       categories is a list of categories
       library is a dictionary, default is None
       lang is a string, default is 'en'"""
    elements = []
    if lang == 'en':
        if categories and not library:
            for category in categories:
                payload = {
                    'title': category.capitalize() + ' Books',
                    'image_url': 'https://m.media-amazon.com/images/I/51SRF4uN1-L._SY346_.jpg',
                    'subtitle': 'I bet you will enjoy reading them.',
                    'default_action': {
                        'type': 'web_url',
                        'url': 'https://www.amazon.com',
                        'webview_height_ratio': 'tall'
                    },
                    'buttons': [
                        {
                            'type':'postback',
                            'title': category.capitalize() + ' Books',
                            'payload': 'DEVELOPER_DEFINED_PAYLOAD'
                        }
                    ]
                }
                elements.append(payload)

        elif categories and library:
            print(categories, name, library)
            for key in library:
                if key == categories[0]:
                    for book in library[key]:
                        payload = {
                            'title': book.capitalize(),
                            'image_url': 'https://m.media-amazon.com/images/I/51SRF4uN1-L._SY346_.jpg',
                            'subtitle': 'I bet you will enjoy reading it.',
                            'default_action': {
                                'type': 'web_url',
                                'url': 'https://www.amazon.com',
                                'webview_height_ratio': 'tall'
                            },
                            'buttons': [
                                {
                                    'type':'postback',
                                    'title': book.capitalize(),
                                    'payload': 'DEVELOPER_DEFINED_PAYLOAD'
                                }
                            ]
                        }
                        elements.append(payload)
    elif lang == 'mm':
        if categories and not library:
            for category in categories:
                if category == 'science':
                    category = 'သိပ္ပံ'
                elif category == 'fiction':
                    category = 'စိတ်ကူးယဉ်'
                elif category == 'drama':
                    category = 'ဒရာမာ'
                payload = {
                    'title': category + 'စာအုပ်များ',
                    'image_url': 'https://m.media-amazon.com/images/I/51SRF4uN1-L._SY346_.jpg',
                    'subtitle': 'I bet you will enjoy reading them.',
                    'default_action': {
                        'type': 'web_url',
                        'url': 'https://www.amazon.com',
                        'webview_height_ratio': 'tall'
                    },
                    'buttons': [
                        {
                            'type':'postback',
                            'title': category + 'စာအုပ်များ',
                            'payload': 'DEVELOPER_DEFINED_PAYLOAD'
                        }
                    ]
                }
                elements.append(payload)

        elif categories and library:
            print(categories, name, library)
            for key in library:
                if key == categories[0]:
                    for book in library[key]:
                        payload = {
                            'title': book.capitalize(),
                            'image_url': 'https://m.media-amazon.com/images/I/51SRF4uN1-L._SY346_.jpg',
                            'subtitle': 'I bet you will enjoy reading it.',
                            'default_action': {
                                'type': 'web_url',
                                'url': 'https://www.amazon.com',
                                'webview_height_ratio': 'tall'
                            },
                            'buttons': [
                                {
                                    'type':'postback',
                                    'title': book.capitalize(),
                                    'payload': 'DEVELOPER_DEFINED_PAYLOAD'
                                }
                            ]
                        }
                        elements.append(payload)

    return elements
